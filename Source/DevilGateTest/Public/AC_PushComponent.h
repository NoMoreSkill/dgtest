// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "..\Source\DevilGateTest/Interactable.h"
#include "AC_PushComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DEVILGATETEST_API UAC_PushComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAC_PushComponent();

protected:

	virtual void BeginPlay() override;
	
	UPROPERTY(EditDefaultsOnly, Category = "Interaction")
		float PushRange;
	UPROPERTY(EditDefaultsOnly, Category = "Interaction")
		float PushSpeed;
	/*UPROPERTY(EditDefaultsOnly, Category = "Interaction")
		AInteractable* CurrentPushObject;*/
	UPROPERTY(EditDefaultsOnly, Category = "Interaction")
		float ForwardMove;
	UPROPERTY(EditDefaultsOnly, Category = "Interaction")
		float CurrentActorRotation;
	/*UPROPERTY(EditDefaultsOnly, Category = "Interaction")
		float PushSpeed;*/
	UPROPERTY(EditDefaultsOnly, Category = "Interaction")
		ACharacter* Character;
	UPROPERTY(EditDefaultsOnly, Category = "Interaction")
		FRotator PushRotation;
	UPROPERTY(EditDefaultsOnly, Category = "Interaction")
		FVector PushVelocity;
	UPROPERTY(EditDefaultsOnly, Category = "Interaction")
		bool IsMovingAnObject;


	void BeginPush(AInteractable* PushableObject);
};
