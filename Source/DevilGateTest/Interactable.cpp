// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactable.h"
#include "Kismet/KismetMathLibrary.h"
#include <DevilGateTest/Public/AC_PushComponent.h>

// Sets default values
AInteractable::AInteractable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
}

// Called when the game starts or when spawned
void AInteractable::BeginPlay()
{
	Super::BeginPlay();
	
}

void AInteractable::Interact(AActor* Character)
{
	IInteractInterface::Interact(Character);
	AInteractable::HandleInteraction(Character);
}

void AInteractable::HandleInteraction(AActor* Character)
{
	UE_LOG(LogTemp, Warning, TEXT("Hello1"));
	FTransform CharacterNewTransform;
	UAC_PushComponent* PushComponent;
	if (IsValid(Character))
	{
		UE_LOG(LogTemp, Warning, TEXT("Hello2"));
		UActorComponent* CharComponent = Character->GetComponentByClass(UAC_PushComponent::StaticClass());
		if (IsValid(CharComponent))
		{
			UE_LOG(LogTemp, Warning, TEXT("Hello3"));
			FVector2D CharLocation;
			CharLocation.X = Character->GetActorLocation().X;
			CharLocation.Y = Character->GetActorLocation().Y;
			int ClosestTransform = GetPushIndex(CharLocation, 150);//����� ������ �������� �� ����������
			if (ClosestTransform > -1)
			{
				UE_LOG(LogTemp, Warning, TEXT("Hello4"));
				FTransform tempTransform = (this->GetActorTransform())*(PushTransform[ClosestTransform]);
				CharacterNewTransform.SetLocation(tempTransform.GetLocation() + (0.0f, 0.0f, 60.0f));
				CharacterNewTransform.SetRotation(tempTransform.GetRotation());
				CharacterNewTransform.SetScale3D(FVector(1.0f, 1.0f, 1.0f));
				Character->SetActorTransform(CharacterNewTransform);
			}
			UE_LOG(LogTemp, Warning, TEXT("Hello"));
		}
		
	}
}

int AInteractable::GetPushIndex(FVector2D CharLocation, float PushRange)
{
	float LowestDistance = 0.0f;
	int ClosestIndex = -1;
	for (int i = 0; i < PushTransform.Num(); i++)
	{
		FVector Vector = UKismetMathLibrary::TransformLocation(this->GetActorTransform(), PushTransform[i].GetLocation());
		FVector2D Vector2D(Vector.X, Vector.Y);
		FVector2D DistVector;
		float Distance;
		DistVector.X = (abs(Vector2D.X) + abs(CharLocation.X)) * (abs(Vector2D.X) + abs(CharLocation.X));
		DistVector.Y = (abs(Vector2D.Y) + abs(CharLocation.Y)) * (abs(Vector2D.Y) + abs(CharLocation.Y));
		Distance = sqrt(DistVector.X + DistVector.Y);
		UE_LOG(LogTemp, Warning, TEXT("Hello"));
		if (PushRange >= Distance)
		{
			LowestDistance = Distance;
			ClosestIndex = i;
		}
	}
	return ClosestIndex;
}
