// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Containers/Array.h"
#include "..\Source\DevilGateTest\Public/InteractInterface.h"
#include "Interactable.generated.h"

UCLASS()
class DEVILGATETEST_API AInteractable : public AActor, public IInteractInterface
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AInteractable();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	virtual void Interact(AActor* Character) override;

	UPROPERTY(EditAnywhere, Category = "Interaction")
		TArray<FTransform> PushTransform;

	void HandleInteraction(AActor* Character);
	
	int GetPushIndex(FVector2D CharLocation, float PushRange);

};
