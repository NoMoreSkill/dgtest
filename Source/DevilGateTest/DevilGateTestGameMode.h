// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DevilGateTestGameMode.generated.h"

UCLASS(minimalapi)
class ADevilGateTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ADevilGateTestGameMode();
};



